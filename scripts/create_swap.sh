# Run from your shell before deployment:
## ssh root@{IP} 'bash -s' < create_swap.sh

touch swap.img
chmod 600 swap.img
dd if=/dev/zero of=/var/swap.img bs=2048k count=1000
mkswap /var/swap.img
fswapon /var/swap.img
swapon /var/swap.img
free

