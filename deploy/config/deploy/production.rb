# server-based syntax
# ======================
# Defines a single server with a list of roles and multiple properties.
# You can define all roles on a single server, or split them:

# server "example.com", user: "deploy", roles: %w{app db web}, my_property: :my_value
# server "example.com", user: "deploy", roles: %w{app web}, other_property: :other_value
# server "db.example.com", user: "deploy", roles: %w{db}

server '159.65.121.147', user: 'root', roles: %w{web app}

set :ssh_options, {
    keys: %w(id_rsa full path),
    forward_agent: true,
    #auth_methods: %w(password) #password _8079d230dcb3046c70eb24faf9
  }