import { ApiService } from '../api.service';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class InstructionService extends ApiService {
  // tslint:disable-next-line:variable-name
  protected _apiMethodToCall: (string) = 'getInstruction';
  // tslint:disable-next-line:variable-name
  protected _apiMethodToCallForDoc: (string) = 'getInstructionDoc';

  public getInstruction() {
    return this.get(this._apiMethodToCall);
  }
  public getDoctorsInstruction() {
    return this.get(this._apiMethodToCallForDoc);
  }
}
