<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

// Get gallery image(s)
Route::get('/getGalleryImages', 'Api\HomeController@getHomeGallery');

// Get certificates
Route::get('/getCertificates', 'Api\HomeController@getCertificates');

// Get photos
Route::get('/getPhotos', 'Api\HomeController@getPhotos');

// Get instruction
Route::get('/getInstruction', 'Api\HomeController@getInstruction');

// Get instruction for doctors
Route::get('/getInstructionDoc', 'Api\HomeController@getInstructionDoc');
