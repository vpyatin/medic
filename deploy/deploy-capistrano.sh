#!/usr/bin/env bash

# error print
error_exit ()
{
    local message="$1"
    echo "${1:-${message}}" 1>&2
    exit 1
}

cd "$( dirname "${BASH_SOURCE[0]}" )"

PROJECT="medsystems"
SERVER="production"

## Other possible cases
## :b:l:r:c:e
while getopts s option
do
    case ${option}
    in
        s) SERVER=${OPTARG};;
    esac
done

if [ -z "${SERVER}" ]; then
    error_exit "Stage isn't set. Please use \"-s <server>\""
fi

#cap ${STAGE} deploy ${BRANCH_ARG} ${LOCAL_REPO_ARG} ${REPO_ARG} ${CLEAR_CACHE_ARG} ${RESTART_DAEMONS_ARG} ${DEPLOY_DIR_ARG} -S base_path=${BASE_PATH}
HOUR=$(date +%H:%M:%S)
echo "[$HOUR] ====== Project ${PROJECT} deploying to ${SERVER}]"
cap ${SERVER} deploy