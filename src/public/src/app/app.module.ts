import {BrowserModule, Title} from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';

// Pages
import { HomeComponent } from './pages/home/home.component';
import { ContactUsComponent } from './pages/contact-us/contact-us.component';
// Additional
import {SlideshowModule} from 'ng-simple-slideshow';
import { CertificatesComponent } from './pages/certificates/certificates.component';
import { ParentsInfoComponent } from './pages/parents-info/parents-info.component';
import { InstructionComponent } from './pages/instruction/instruction.component';
import { CtgSigmafonComponent } from './pages/ctg-sigmafon/ctg-sigmafon.component';
import { PhotoComponent } from './pages/photo/photo.component';
import { InstructionDoctorComponent } from './pages/instruction-doctor/instruction-doctor.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    ContactUsComponent,
    CertificatesComponent,
    ParentsInfoComponent,
    InstructionComponent,
    CtgSigmafonComponent,
    PhotoComponent,
    InstructionDoctorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SlideshowModule,
    HttpClientModule
  ],
  providers: [
    Title
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
