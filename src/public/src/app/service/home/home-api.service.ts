import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';

@Injectable({
  providedIn: 'root'
})

export class HomeApiService extends ApiService {
    // tslint:disable-next-line:variable-name
    protected _apiMethodToCall: (string) = 'getGalleryImages';

    public getHomeGallery() {
        return this.get(this._apiMethodToCall);
    }
}
