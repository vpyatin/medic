import { Component, OnInit } from '@angular/core';
import {InstructionService} from '../../service/instruction/instruction.service';

@Component({
  selector: 'app-instruction-doctor',
  templateUrl: './instruction-doctor.component.html',
  styleUrls: ['./instruction-doctor.component.scss']
})
export class InstructionDoctorComponent implements OnInit {
  public instructionText;
  public instructionGraphic;
  public instructions;

  constructor(protected instructionService: InstructionService) { }

  ngOnInit() {
    this.instructionService.getDoctorsInstruction().subscribe((response) => {
      if (response.body) {
        this.instructions = response.body;
        this.instructionGraphic = this.instructions[0];
        this.instructionText = this.instructions[1];
      }
    });
  }
}
