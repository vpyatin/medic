# Run from your shell before deployment:
## ssh root@{IP/HOST} 'bash -s' < deploy_environment_remote.sh

#### Install NGINX
apt-get update
apt-get -y install curl nano vim
apt-get -y install nginx

# Install utilities
apt-get update && apt-get install zip unzip wget apt-transport-https -y

debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'
apt-get install mariadb-server -y

#### Install php
mkdir -p /var/run

apt-get --no-install-recommends --no-install-suggests -q -y install gconf-service libasound2 libatk1.0-0 libc6 libcairo2 libcups2 libdbus-1-3 libexpat1 libfontconfig1 libgcc1 libgconf-2-4 libgdk-pixbuf2.0-0 libglib2.0-0 libgtk-3-0 libnspr4 libpango-1.0-0 libpangocairo-1.0-0 libstdc++6 libx11-6 libx11-xcb1 libxcb1 libxcomposite1 libxcursor1 libxdamage1 libxext6 libxfixes3 libxi6 libxrandr2 libxrender1 libxss1 libxtst6 ca-certificates fonts-liberation libnss3 lsb-release xdg-utils wget

wget -qO - https://packages.sury.org/php/apt.gpg | apt-key add - \
    && echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/php.list \
    && apt-get update \
    && apt-get install --no-install-recommends --no-install-suggests -q -y \
            apt-utils \
            curl \
            nano \
            python3-pip \
            python3-setuptools \
            php8.0-fpm \
            php8.0-cli \
            php8.0-dev \
            php8.0-common \
            php8.0-opcache \
            php8.0-readline \
            php8.0-mbstring \
            php8.0-curl \
            php8.0-memcached \
            php8.0-imagick \
            php8.0-mysql \
            php8.0-pgsql \
            php8.0-intl \
            php8.0-xml \
            php8.0-bcmath \
            php8.0-gd \
            php8.0-soap \
            php8.0-redis

apt-get clean && apt-get --yes --quiet autoremove

curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

#### Install node and angular
curl -sL https://deb.nodesource.com/setup_14.x | bash -
apt-get install nodejs -y
npm install -g @angular/cli -y


