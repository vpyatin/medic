<?php
/**
 * Migration homepage gallery
 *
 * @category  App
 * @package   App
 * @author    Vitaliy Pyatin <mail.pyvil@gmail.com>
 * @cppyright Vitaliy Pyatin
 */

use App\Api\Home;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HomepageGallery extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            Home::TABLE_NAME,
            function (Blueprint $table) {
                $table->bigIncrements(Home::ID);
                $table->string(Home::PATH);
                $table->string(Home::TYPE, 100);
                $table->string(Home::TITLE);
                $table->bigInteger(Home::ORDER);
                $table->timestamps();
                $table->engine = 'InnoDB';
                $table->charset = 'utf8';
                $table->collation = 'utf8_unicode_ci';
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Home::TABLE_NAME);
    }
}
