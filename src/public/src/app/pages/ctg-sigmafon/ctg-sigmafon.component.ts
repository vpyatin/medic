import { Component, OnInit } from '@angular/core';
import {HomeApiService} from "../../service/home/home-api.service";
import {DomSanitizer} from "@angular/platform-browser";
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-ctg-sigmafon',
  templateUrl: './ctg-sigmafon.component.html',
  styleUrls: ['./ctg-sigmafon.component.scss']
})
export class CtgSigmafonComponent implements OnInit {

  /**
   * Gallery to display (video images)
   */
  public galleryToDisplayVideo;
  public galleryToDisplayImages: any;

  public mainImage = null;

  constructor(
      protected homeService: HomeApiService,
      private sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.mainImage = environment.baseApiUrl + '/images/cache/catalog/product/I/M/IMG_4919.JPG';
    this.homeService.getHomeGallery().subscribe((response) => {
      if (response.body) {
        this.galleryToDisplayVideo = response.body;
        for (let i = 0; i <= this.galleryToDisplayVideo.length - 1; i++) {
          this.galleryToDisplayVideo[i] = this.sanitizer.bypassSecurityTrustResourceUrl(this.galleryToDisplayVideo[i].path);
        }
      }
    });
  }
}
