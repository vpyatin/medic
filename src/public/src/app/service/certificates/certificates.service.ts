import { ApiService } from '../api.service';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class CertificatesService extends ApiService {
  // tslint:disable-next-line:variable-name
  protected _apiMethodToCall: (string) = 'getCertificates';

  public getCertificates() {
    return this.get(this._apiMethodToCall);
  }
}
