<?php
/**
 * Api controller for homepage
 *
 * @category  App
 * @package   App\Http\Controllers\Api
 * @author    Vitaliy Pyatin <mail.pyvil@gmail.com>
 * @cppyright Vitaliy Pyatin
 */

namespace App\Http\Controllers\Api;

use App\Api\Home;
use App\Data;
use App\Http\Controllers\ControllerJson;

/**
 * Class HomeController
 *
 * @package App\Http\Controllers\Api
 */
class HomeController extends ControllerJson
{
    /**
     * Get gallery images
     *
     * @return string
     */
    public function getHomeGallery()
    {
        return response()->json(Home::getGallery());
    }

    /**
     * Get certificates
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCertificates()
    {
        $originalPhotos = [
            'certificates/IMG_3768.JPG',
            'certificates/IMG_3769.JPG'
        ];
        $images = [];
        foreach ($originalPhotos as $photo) {
            $images[] = [
                'original' => Data::getFileUrl($photo),
                'resize'   => Data::resizeImage($photo, 300, 420, false)
            ];
        }

        return response()->json($images);
    }

    /**
     * Get photos
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPhotos()
    {
        $originalPhotos = [
            'catalog/product/I/M/IMG_4919.JPG',
            'catalog/product/I/M/IMG_4920.JPG',
            'catalog/product/I/M/IMG_4923.JPG',
            'catalog/product/I/M/IMG_4924.JPG',
            'catalog/product/I/M/IMG_4926.JPG',
            'catalog/product/I/M/IMG_4927.JPG',
            'catalog/product/I/M/IMG_4928.JPG',
            'catalog/product/I/M/IMG_4929.JPG',
            'catalog/product/I/M/IMG_4930.JPG',
            'catalog/product/I/M/IMG_4931.JPG',
            'catalog/product/I/M/IMG_4932.JPG',
            'catalog/product/I/M/IMG_4934.JPG',
            'catalog/product/I/M/IMG_4935.JPG',
        ];
        $images = [];
        foreach ($originalPhotos as $photo) {
            $images[] = [
                'original' => Data::getFileUrl($photo),
                'resize'   => Data::resizeImage($photo, 300, 300, false)
            ];
        }

        return response()->json($images);
    }

    /**
     * Get instructions
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getInstruction()
    {
        return response()->json([
            Data::getFileUrl('data/catalog/product/instrukcia_koristuvacha_Sigmafon_UA.docx'),
            Data::getFileUrl('data/catalog/product/instrukcja_kolor_UA.pdf'),
            Data::getFileUrl('data/catalog/product/presentation.pptx'),
        ]);
    }

    /**
     * Get instructions for doctors
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getInstructionDoc()
    {
        return response()->json([
            Data::getFileUrl('data/catalog/product/14033.pdf'),
            Data::getFileUrl('data/catalog/product/14063.pdf')
        ]);
    }
}
