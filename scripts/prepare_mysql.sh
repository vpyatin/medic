# Run from your shell before deployment:
## ssh root@68.183.20.147 'bash -s' < prepare_mysql.sh

mysql -uroot -proot -e "CREATE DATABASE production /*\!40100 DEFAULT CHARACTER SET utf8 */;"
mysql -uroot -proot -e "CREATE USER 'medsystems'@'%' IDENTIFIED BY 'aCuJw4QP78upd';"
mysql -uroot -proot -e "GRANT ALL PRIVILEGES ON production.* TO 'medsystems'@'%' IDENTIFIED BY 'aCuJw4QP78upd';"
mysql -uroot -proot -e "FLUSH PRIVILEGES;"

