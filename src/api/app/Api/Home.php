<?php
/**
 * Api model for homepage
 *
 * @category  App
 * @package   App\Api
 * @author    Vitaliy Pyatin <mail.pyvil@gmail.com>
 * @cppyright Vitaliy Pyatin
 */

namespace App\Api;

use App\Data;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Home
 *
 * @package App\Api
 */
class Home extends Model
{
    /**
     * Table name
     */
    const TABLE_NAME = 'homepage_gallery';

    /**#@+
     * Columns
     */
    const ID = 'id';
    const PATH = 'path';
    const TYPE = 'type';
    const TITLE = 'title';
    const ORDER = 'order';
    /**#@-*/

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = self::TABLE_NAME;

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        self::ORDER => 1
    ];

    /**
     * Get gallery images
     *
     * @return array
     */
    public static function getGallery()
    {
        $images = self::query()->where(static::TYPE, '=', 'video')->get([static::TYPE, static::PATH]);
        foreach ($images as &$image) {
            if ($image[static::TYPE] == 'image') {
                $image[static::PATH] = Data::getFileUrl($image[static::PATH]);
            }
        }

        return $images;
    }
}
