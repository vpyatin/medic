# config valid for current version and patch releases of Capistrano
lock "~> 3.16.0"

set :application, 'medsystems'
set :repo_url, 'git@gitlab.com:pyvil/medic.git'

# Default branch is :master
set :branch, ENV["branch"] || "master"
set :deploy_to, "/var/www/medsystems"
set :laravel_dotenv_file, "/var/www/medsystems/conf/laravel/prod/.env"
# Default value for keep_releases is 5
set :keep_releases, 1

namespace :deploy do

  desc 'Get stuff ready prior to symlinking'
  task :compile_assets do
    on roles(:app), in: :sequence, wait: 1 do
      execute "rm -rf /etc/nginx/nginx.conf"
      execute "rm -rf /etc/nginx/conf.d/default.conf"
      execute "ln -s #{release_path}/conf/nginx/prod/nginx.conf /etc/nginx/nginx.conf"
      execute "ln -s #{release_path}/conf/nginx/prod/default.conf /etc/nginx/conf.d/default.conf"
      execute "cd #{release_path}/src/api/ && composer install"
      execute "cp #{release_path}/conf/laravel/prod/.env #{release_path}/src/api/.env"
      execute "cd #{release_path}/src/api/ && php artisan migrate --force"
      # execute "cd #{release_path}/src/api/ && php artisan db:seed --force"

      execute "cd #{release_path}/src/public && npm install"
      execute "rm -rf #{release_path}/src/public/proxy.conf.json"
      execute "cp #{release_path}/conf/angular/prod/proxy.conf.json #{release_path}/src/public/proxy.conf.json"
      execute "cd #{release_path}/src/public && ng build --prod"

      execute "chown www-data:www-data -R #{release_path}/src"
      execute "service nginx restart"
    end
  end

  after :updated, :compile_assets

end