import { Component, OnInit } from '@angular/core';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  /**
   * Chosen category
   */
  // tslint:disable-next-line:variable-name
  protected _chosenCategory: string = null;

  /**
   * Categories
   */
  // tslint:disable-next-line:variable-name
  protected _categories = [
    {
      name: 'Інформація батькам',
      path: 1,
      url_key: 'parents-info',
      order: 1,
      parent_id: 0,
      is_active: 1
    },
    {
      name: 'Інструкція для батьків',
      path: 1,
      url_key: 'instruction',
      order: 2,
      parent_id: 0,
      is_active: 1
    },
    {
      name: 'Інструкція для лікарів',
      path: 1,
      url_key: 'instruction-doctor',
      order: 3,
      parent_id: 0,
      is_active: 1
    },
    {
      name: 'КТГ Сигмафон',
      path: 1,
      url_key: 'ctg-sigmafon',
      order: 4,
      parent_id: 0,
      is_active: 1
    },
    {
      name: 'Сертфиікати',
      path: 1,
      url_key: 'certificates',
      order: 5,
      parent_id: 0,
      is_active: 1
    },
    {
      name: 'Фото',
      path: 1,
      url_key: 'photo',
      order: 6,
      parent_id: 0,
      is_active: 1
    },
    {
      name: 'Контакти',
      path: 1,
      url_key: 'contact-us',
      order: 7,
      parent_id: 0,
      is_active: 1
    }
  ];

  constructor(
    protected titleService: Title
  ) {}

  /**
   * Set page <title></title>
   *
   * @param title new title (string)
   * @param urlKey url key for category
   */
  setPageParameters(title: string, urlKey: string) {
    this.titleService.setTitle(title);
    this._chosenCategory = urlKey;
  }

  /**
   * Get categories
   */
  get categories(): {}[] {
    return this._categories;
  }

  /**
   * Get chosen category to mark in css
   */
  get chosenCategory(): string {
    let result = this._chosenCategory;
    if (result === null) {
        const paths = window.location.pathname.split('/').filter((item) => item !== '' && item !== null);
        result = paths[0];
    }

    return result;
  }

  ngOnInit(): void {
  }
}
