<?php
/**
 * Seed model for languages
 *
 * @category  App
 * @package   App
 * @author    Vitaliy Pyatin <mail.pyvil@gmail.com>
 * @cppyright Vitaliy Pyatin
 */

use App\Lang;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Class LanguagesSeeder
 */
class LanguagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rows = [
            Lang::CODE_ENG => 'English',
            Lang::CODE_UA  => 'Українська',
            Lang::CODE_FR  => 'France',
            Lang::CODE_PL  => 'Polska',
            Lang::CODE_RU  => 'Русский'
        ];
        foreach ($rows as $code => $name) {
            DB::table(Lang::TABLE_NAME)->insert([
                Lang::CODE       => $code,
                Lang::NAME       => $name,
                Lang::IS_DEFAULT => Lang::DEFAULT_LANGUAGE == $code ? 1 : 0
            ]);
        }
    }
}
