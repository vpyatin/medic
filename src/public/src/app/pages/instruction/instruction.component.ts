import { Component, OnInit } from '@angular/core';
import {InstructionService} from '../../service/instruction/instruction.service';

@Component({
  selector: 'app-instruction',
  templateUrl: './instruction.component.html',
  styleUrls: ['./instruction.component.scss']
})
export class InstructionComponent implements OnInit {
  public colorInstruction;
  public docInstruction;
  public presentation;
  public instructions;

  constructor(protected instructionService: InstructionService) { }

  ngOnInit() {
    this.instructionService.getInstruction().subscribe((response) => {
      if (response.body) {
        this.instructions = response.body;
        this.docInstruction = this.instructions[0];
        this.colorInstruction = this.instructions[1];
        this.presentation = this.instructions[2];
      }
    });
  }
}
