import { Component, OnInit } from '@angular/core';
import {HomeApiService} from "../../service/home/home-api.service";
import {DomSanitizer} from "@angular/platform-browser";
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-parents-info',
  templateUrl: './parents-info.component.html',
  styleUrls: ['./parents-info.component.scss']
})
export class ParentsInfoComponent implements OnInit {

  /**
   * Gallery to display (video images)
   */
  public galleryToDisplayVideo;
  public galleryToDisplayImages: any;

  public mainImage = null;

  constructor(
      protected homeService: HomeApiService,
      private sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.mainImage = environment.baseApiUrl + '/images/pregnant_family.jpg';
    this.homeService.getHomeGallery().subscribe((response) => {
      if (response.body) {
        this.galleryToDisplayVideo = response.body;
        for (let i = 0; i <= this.galleryToDisplayVideo.length - 1; i++) {
          this.galleryToDisplayVideo[i] = this.sanitizer.bypassSecurityTrustResourceUrl(this.galleryToDisplayVideo[i].path);
        }
      }
    });
  }

}
