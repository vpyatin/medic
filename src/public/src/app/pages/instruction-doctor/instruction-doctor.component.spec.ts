import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstructionDoctorComponent } from './instruction-doctor.component';

describe('InstructionDoctorComponent', () => {
  let component: InstructionDoctorComponent;
  let fixture: ComponentFixture<InstructionDoctorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstructionDoctorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstructionDoctorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
