<?php
/**
 * Base data
 *
 * @category  App
 * @package   App
 * @author    Vitaliy Pyatin <mail.pyvil@gmail.com>
 * @cppyright Vitaliy Pyatin
 */

namespace App;

use Intervention\Image\ImageManager;

/**
 * Class Data
 *
 * @package App
 */
class Data
{
    /**
     * Default API port
     */
    const DEFAULT_API_PORT = '8000';

    /**#@+
     * Folrder names
     */
    const IMAGE_FOLDER_NAME = 'images';
    /**#@-*/

    /**
     * Get file path
     *
     * @param string $fileName
     *
     * @return string
     */
    public static function getFilePath($fileName = null)
    {
        if (!$fileName) {
            return null;
        }
        $path = static::getImageFolderPath() . $fileName;

        return $path;
    }

    /**
     * Get file url
     *
     * @param string $fileName
     *
     * @return string
     */
    public static function getFileUrl($fileName = null)
    {
        if (!$fileName) {
            return null;
        }
        if (strpos($fileName, 'http') !== false) {
            return $fileName;
        }
        $path = static::getImageFolderUrl() . $fileName;

        return $path;
    }

    /**
     * Get image folder
     *
     * @return string
     */
    public static function getImageFolderUrl()
    {
        return rtrim(url('/'), DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR
        . static::IMAGE_FOLDER_NAME . DIRECTORY_SEPARATOR;
    }

    /**
     * Get image folder path
     *
     * @return string
     */
    public static function getImageFolderPath()
    {
        return rtrim(public_path('/'), DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . static::IMAGE_FOLDER_NAME
            . DIRECTORY_SEPARATOR;
    }

    /**
     * Resize image
     *
     * @param string $image
     * @param int $width
     * @param int $height
     * @param bool $forceRecreate - true if need to rewrite file, false otherwise
     *
     * @return string
     */
    public static function resizeImage($image, $width = 200, $height = 200, $forceRecreate = false)
    {
        $imageCacheDirectory = static::getImageFolderPath() . 'cache' . DIRECTORY_SEPARATOR;
        $cachedFilePath = $imageCacheDirectory . $image;
        $cachedUrl = static::getImageFolderUrl() . 'cache' . DIRECTORY_SEPARATOR . $image;
        if (!file_exists($cachedFilePath) || $forceRecreate) {
            if (file_exists($cachedFilePath)) {
                unlink($cachedFilePath);
            }
            $imageManager = new ImageManager(array('driver' => 'imagick'));
            $image = $imageManager->make(Data::getFilePath($image))->resize($width, $height);
            $image->save($cachedFilePath);
        }

        return $cachedUrl;
    }
}
