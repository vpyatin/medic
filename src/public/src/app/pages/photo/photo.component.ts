import { Component, OnInit } from '@angular/core';
import {PhotoService} from '../../service/photo/photo.service';

@Component({
  selector: 'app-photo',
  templateUrl: './photo.component.html',
  styleUrls: ['./photo.component.scss']
})
export class PhotoComponent implements OnInit {

  public photos;

  constructor(protected photoService: PhotoService) { }

  ngOnInit() {
    this.photoService.getPhotos().subscribe((response) => {
      if (response.body) {
        this.photos = response.body;
      }
    });
  }

}
