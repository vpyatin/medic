<?php
/**
 * Seed model for homepage
 *
 * @category  App
 * @package   App\Api
 * @author    Vitaliy Pyatin <mail.pyvil@gmail.com>
 * @cppyright Vitaliy Pyatin
 */

use App\Api\Home as ModelHome;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Class HomeSeeder
 */
class HomeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table(ModelHome::TABLE_NAME)->insert([
            ModelHome::TITLE      => 'Slide 1',
            ModelHome::PATH       => 'https://www.youtube.com/embed/ZQWAmwbUTHY',
            ModelHome::TYPE       => 'video',
            ModelHome::ORDER      => 1,
            ModelHome::CREATED_AT => date('Y-m-d H:i:s', time()),
            ModelHome::UPDATED_AT => date('Y-m-d H:i:s', time()),
        ]);
    }
}
